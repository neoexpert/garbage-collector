test: src/heap.c src/test.c
	cc -g src/util/*.c src/heap.c src/test.c -lm -o test
demo: src/heap.c src/test.c
	cc -D VERBOSE -g src/util/*.c src/heap.c src/heaplogger.c src/test.c -lm -o demo
itest: src/heap.c src/itest.c
	cc -D VERBOSE -g src/util/*.c src/heap.c src/heaplogger.c src/itest.c -lm -o itest

heap.js: src/heap.c src/wasm.c
	emcc -Oz src/util/*.c src/wasm.c src/heap.c -o heap.js -s ASSERTIONS=0 -s ALLOW_MEMORY_GROWTH=1 -s EXPORTED_FUNCTIONS='["_createStack", "_createHeap", "_markAliveExtern", "_createObj"]' -s EXPORTED_RUNTIME_METHODS='["ccall","cwrap"]'
