#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <string.h>
#include <math.h>

#include <sys/ioctl.h>
#include <unistd.h>
#include <time.h>

#include "util/iarraylist.h"
#include "heap.h"

#ifdef VERBOSE
#include "heaplogger.h"
#endif

void deleteHeap(Heap * heap){
	deleteIArrayList(heap->refsList0);
	deleteIArrayList(heap->refsList1);
	deleteIArrayList(heap->roots);
	free(heap->heap);
	free(heap);
}

Heap * newHeap(uint32_t edenCapacity, void ** heapPtr, BeforeGC gcCallback){
	Heap * heap=malloc(sizeof(Heap));
	if(heap==NULL){
		fprintf(stderr,"heap == NULL\n");
		exit(-1);
	}
	heap->beforeGC=gcCallback;
	heap->edenCapacity = edenCapacity;
	uint32_t survivorCapacity = edenCapacity * 16;
	heap->minReserve = edenCapacity + survivorCapacity + 8;
	heap->survivorCapacity = survivorCapacity;
	heap->survivorSize = 0;
	heap->survivorFull = false;
	heap->survivorThreshold=8;
	heap->s1=0;
	uint32_t youngCapacity = edenCapacity + survivorCapacity * 2;
	heap->youngCapacity = youngCapacity;
	uint64_t heapCapacity = heap->minReserve * 2 + youngCapacity + 4;
	if(heapCapacity > UINT32_MAX){
		fprintf(stderr,"error heapCapacity > 4GB");
		exit(-1);
	}
	heap->capacity = heapCapacity;
	heap->heap = malloc(heapCapacity);
	if(heap->heap==NULL){
		fprintf(stderr,"could not allocate memory: %" PRIu64 " bytes\n", heapCapacity);
		exit(-1);
	}
	heap->ptr = heapPtr;
	if(heap->ptr!=NULL)
		*(heap->ptr) = heap->heap;
	heap->oldGenSize = 4;
	heap->gc_cycle = 0;
	heap->roots = newIArrayList(16);
	heap->oldGenCapacity = heapCapacity - youngCapacity;
	heap->genThreshold = heap->oldGenCapacity;
	heap->pointer = heapCapacity - edenCapacity;
	heap->copy = NULL;
	heap->refsList0 = newIArrayList(16);
	heap->refsList1 = newIArrayList(16);
#ifdef VERBOSE
	logHeap(heap);
#endif
	return heap;
}

uint32_t addRoot(Heap * heap){
	uint32_t index=heap->roots->size;
	IArrayList_add(heap->roots, 0);
	return index;
}

void setRoot(Heap * heap, uint32_t index, uint32_t ref){
	*(uint32_t *)(heap->roots->values+index)=ref;
}

uint32_t getRoot(Heap * heap, uint32_t index){
	return	*(uint32_t *)(heap->roots->values+index);
}

void growHeap(Heap * heap, uint32_t isize){
	uint64_t capacity=heap->capacity;
	capacity = (capacity * 3) / 2 + heap->minReserve + isize;
	if(capacity > UINT32_MAX){
		capacity = UINT32_MAX;
		uint64_t oldGenCapacity = capacity - heap->youngCapacity;
		if(oldGenCapacity - heap->minReserve - heap->oldGenSize <= isize){
			fprintf(stderr,"error while growing heap: 4GB limit exceeded");
			exit(-1);
		}
	}
	heap->capacity = capacity;
	void * newHeap = realloc(heap->heap, heap->capacity);
	if(newHeap == NULL){
		fprintf(stderr,"error while growing heap to: %s\n", humanSize(heap->capacity));
		exit(-1);
	}
	else{
#ifdef VERBOSE
		fprintf(stdout,"growing heap to: %s\n", humanSize(heap->capacity));
#endif
		if(heap->heap != newHeap){
			heap->heap=newHeap;
			if(heap->ptr!=NULL)
				*(heap->ptr) = newHeap;
		}	
		heap->oldGenCapacity = heap->capacity - heap->youngCapacity;
		heap->genThreshold = heap->oldGenCapacity;
		heap->pointer = heap->capacity - heap->edenCapacity;
	}
}

void shrinkHeap(Heap * heap){
	uint64_t capacity = heap->oldGenSize + heap->minReserve * 2 + heap->youngCapacity;
	if(capacity > UINT32_MAX){
		capacity = UINT32_MAX;
		uint64_t oldGenCapacity = capacity - heap->youngCapacity;
		if(oldGenCapacity - heap->minReserve - heap->oldGenSize <= 0){
			fprintf(stderr,"error while growing heap: 4GB limit exceeded");
			exit(-1);
		}
	}
	heap->capacity = capacity;
	void * newHeap = realloc(heap->heap, heap->capacity);
	if(newHeap == NULL){
		fprintf(stderr,"error while growing heap to: %s\n", humanSize(heap->capacity));
		exit(-1);
	}
	else{
#ifdef VERBOSE
		fprintf(stdout,"growing heap to: %s\n", humanSize(heap->capacity));
#endif
		if(heap->heap != newHeap){
			heap->heap=newHeap;
			if(heap->ptr!=NULL)
				*(heap->ptr) = newHeap;
		}	
		heap->oldGenCapacity = heap->capacity - heap->youngCapacity;
		heap->genThreshold = heap->oldGenCapacity;
		heap->pointer = heap->capacity - heap->edenCapacity;
	}
}

uint32_t copyObj(Heap * heap, uint32_t ref, Object * o){
	uint32_t size = o->size;
	uint32_t newRef = heap->copySize;
	memcpy(heap->copy + newRef, o, size);
	o->newAddr = newRef;
	o->size = 0;
	heap->copySize += size;
	return newRef;
}

void markAlive(Heap * heap, uint32_t * ref){
	Object * o=heap->heap + *ref;
	if(o->size == 0){
		*ref=o->newAddr;
		return;
	}
	uint32_t newRef = copyObj(heap, *ref, o);
	*ref = newRef;
	IArrayList_add(heap->refsList, newRef);
}

void full_gc(Heap * heap){
#ifdef VERBOSE
	fprintf(stdout,"\nfull_gc %s -> ", humanSize(heap->oldGenSize));
#endif
	heap->copy = malloc(heap->capacity);
	if(heap->copy==NULL){
		fprintf(stderr,"could not allocate memory (heap->copy): %u bytes\n", heap->capacity);
		exit(-1);
	}
	heap->copySize = 4;
	uint32_t len = heap->roots->size;
	uint32_t * roots = heap->roots->values;
	
	//two lists used to store refs to process
	uint8_t currentAL=0;
	IArrayList * refsList0 = heap->refsList0;
	IArrayList * refsList1 = heap->refsList1;
	refsList0->size=0;
	refsList1->size=0;
	IArrayList * refsList = refsList0;
	heap->refsList = refsList;
	if(heap->beforeGC != NULL){
		heap->beforeGC(heap, &markAlive);
	}
	for(uint32_t i=0;i<len;++i){
		uint32_t ref=roots[i];
		if(ref!=0){
			markAlive(heap, &roots[i]);
		}
	}

	while(refsList->size > 0){
		uint32_t * refs = refsList->values;
		uint32_t len = refsList->size;
		if(currentAL == 0){
			refsList = refsList1;
			heap->refsList = refsList;
			currentAL = 1;
		}
		else{
			refsList = refsList0;
			heap->refsList = refsList;
			currentAL = 0;
		}
		refsList->size = 0;

		for(uint32_t i = 0; i < len; ++i){
			uint32_t ref = refs[i];
			Object * o = heap->copy + ref;
			uint16_t refsCount = o->refsCount;
			uint32_t * refs = o->refs;
			for(uint16_t i = 0; i < refsCount; ++i){
				uint32_t ref = refs[i];
				if(ref != 0){
					markAlive(heap, &refs[i]);
				}
			}
		}
	}

	free(heap->heap);
	heap->heap = heap->copy;
	heap->oldGenSize = heap->copySize;
	heap->copy = NULL;
	heap->copySize = 4;
	if(heap->oldGenCapacity - heap->oldGenSize <= heap->minReserve){
#ifdef VERBOSE
		fprintf(stdout,"realloc 1\n");
#endif
		growHeap(heap, 0);
	}
	else 
		if(heap->capacity - heap->minReserve*8 - heap->oldGenSize > 0)
			shrinkHeap(heap);
	heap->pointer = heap->capacity - heap->edenCapacity;
#ifdef VERBOSE
		fprintf(stdout,"%s\n", humanSize(heap->oldGenSize));
#endif
}

uint32_t copyToOldGen(Heap * heap, uint32_t ref, Object * o){
	if(ref >= heap->genThreshold){
		uint32_t size=o->size;
		uint32_t newRef;
		if(!heap->survivorFull && o->survived < heap->survivorThreshold){
			newRef = heap->gcTarget;
			heap->gcTarget += size;
			heap->survivorSize += size;
			heap->survivorFull = heap->survivorSize >= heap->survivorCapacity;
			if(heap->survivorFull){
				heap->survivorSize -= size;
				return copyToOldGen(heap, ref, o);
			}
			++o->survived;
		}
		else{
			newRef = heap->oldGenSize;
			heap->oldGenSize += size;
		}
		memcpy(heap->heap + newRef, o , size);

		o->newAddr = newRef;
		o->size = 0;
		ref=newRef;
	}
	return ref;
}

void markAliveMinor(Heap * heap, uint32_t * ref){
			Object * o=heap->heap + *ref;
			if(o->gc_cycle==heap->gc_cycle)
				return;
			if(o->size == 0){
				*ref = o->newAddr;
				return;
			}
			uint32_t newRef = copyToOldGen(heap, *ref, o);
			if(newRef != *ref){
				*ref = newRef;
				o = heap->heap + newRef;
			}
			o->gc_cycle = heap->gc_cycle;
			IArrayList_add(heap->refsList, newRef);
}

void minor_gc(Heap * heap){
#ifdef VERBOSE
		//fprintf(stdout,"minor_gc ");
#endif
	++heap->gc_cycle;
	uint32_t len = heap->roots->size;
	uint32_t * roots = heap->roots->values;
	
	IArrayList * refsList0 = heap->refsList0;
	IArrayList * refsList1 = heap->refsList1;
	refsList0->size = 0;
	refsList1->size = 0;
	uint8_t currentAL = 0;
	IArrayList * refsList = refsList0;
	heap->refsList=refsList;
	if(heap->beforeGC!=NULL){
		heap->beforeGC(heap, &markAliveMinor);	
	}
	for(uint32_t i = 0; i < len; ++i){
		uint32_t ref=roots[i];
		if(ref != 0){
			markAliveMinor(heap, &roots[i]);
		}
	}

	while(refsList->size > 0){
		uint32_t * refs = refsList->values;
		uint32_t len = refsList->size;
		if(currentAL == 0){
			refsList = refsList1;
			heap->refsList = refsList;
			currentAL = 1;
		}
		else{
			refsList = refsList0;
			heap->refsList = refsList;
			currentAL = 0;
		}
		refsList->size = 0;

		for(uint32_t i = 0; i < len; ++i){
			uint32_t ref = refs[i];
			Object * o = heap->heap + ref;
			uint16_t refsCount = o->refsCount;
			uint32_t * refs = o->refs;
			for(uint16_t i = 0; i < refsCount; ++i){
				uint32_t subRef = refs[i];
				if(subRef != 0){
					markAliveMinor(heap, &refs[i]);
				}
			}
		}
	}

#ifdef VERBOSE
		//fprintf(stdout,"%s\n", humanSize(heap->oldGenSize));
		//fprintf(stdout,"\n");
		//logHeap(heap);
#endif
		/*
	if(heap->oldGenCapacity - heap->oldGenSize <= heap->minReserve){
		gc(heap);
		heap->pointer = heap->capacity - heap->edenCapacity;
		return true;
	}*/
	heap->pointer = heap->capacity - heap->edenCapacity;
	//return false;
}

uint32_t gc(Heap * heap, uint32_t size){
	uint32_t addr;
	uint32_t sOffset=heap->oldGenCapacity + (heap->survivorCapacity * heap->s1);
	heap->survivorOffset=sOffset;
	heap->gcTarget = sOffset;
	heap->survivorSize = 0;
	//heap->gcTarget += heap->survivorSize;
	minor_gc(heap);
	if(heap->survivorFull){
		minor_gc(heap);
		heap->survivorFull = false;
	}
	else{
		heap->survivorSize = heap->gcTarget - sOffset;
		if(heap->survivorSize < heap->survivorCapacity / 2){
			if(heap->survivorThreshold < 255)
				heap->survivorThreshold++;
		}
		else{
			if(heap->survivorThreshold > 1)
				heap->survivorThreshold--;
		}
	}

	uint8_t s1=heap->s1;
	if(s1==0)
		heap->s1 = 1;
	else
		heap->s1 = 0;

	bool fullGC = false;
	if(heap->oldGenCapacity - heap->oldGenSize <= heap->minReserve){
		full_gc(heap);
		fullGC=true;
		if(heap->oldGenCapacity - heap->oldGenSize <= heap->minReserve){
			growHeap(heap, size);
		}
	}

	if(size > heap->edenCapacity){
		if(heap->survivorCapacity - heap->survivorSize >= size){
			if(heap->survivorThreshold > 8)
				heap->survivorThreshold /= 2;
			addr = heap->oldGenCapacity + heap->survivorCapacity * s1 + heap->survivorSize;
			heap->survivorSize += size;
			return addr;
		}
		if(!fullGC)
			full_gc(heap);
		//printf("size %d oldgencap: %d, oldgenSize: %d\n", size, heap->oldGenCapacity, heap->oldGenSize );
		if(heap->oldGenCapacity - heap->minReserve - heap->oldGenSize <= size){
			fprintf(stdout,"realloc 3\n");
			growHeap(heap, size);
		}
		addr=heap->oldGenSize;
		heap->oldGenSize+=size;
	}
	else{
		addr=heap->pointer;
		heap->pointer += size;
	}
	return addr;
}

uint32_t _new(Heap * heap, uint16_t refsCount, uint32_t payload){
	uint32_t size = sizeof(Object) + refsCount * 4 + payload;
	size = ((size + 3 ) >>  2) << 2;
	uint32_t addr;
	if(heap->pointer + size >= heap->capacity){
		addr=gc(heap, size);
	}
	else{
		addr=heap->pointer;
		heap->pointer += size;
	}
	Object * o = heap->heap + addr;
	memset(o->refs, 0, refsCount*4);
	o->size=size;
	o->newAddr=0;
	o->gc_cycle=heap->gc_cycle;
	o->survived=0;
	o->refsCount=refsCount;
#ifdef VERBOSE
	logHeap(heap);
#endif
	return addr;
}

void setRef(Heap * heap, uint32_t pointer, uint16_t index, uint32_t ref){
	Object * o=heap->heap+pointer;
	o->refs[index]=ref;
}

uint32_t getRef(Heap * heap, uint32_t pointer, uint16_t index){
	Object * o=heap->heap+pointer;
	return o->refs[index];
}

void set32(Heap * heap, uint32_t pointer, uint32_t off, uint32_t value){
	Object * o=heap->heap+pointer;
	uint32_t payloadoffset=pointer + sizeof(Object) + o->refsCount * 4;
	payloadoffset += off;
	*((uint32_t *)(heap->heap + payloadoffset))=value;
}

uint32_t get32(Heap * heap, uint32_t pointer, uint32_t off){
	Object * o=heap->heap+pointer;
	uint32_t payloadoffset=pointer + sizeof(Object) + o->refsCount * 4;
	payloadoffset += off;
	return *((uint32_t *)(heap->heap+payloadoffset));
}

void * getPayLoad(Heap * heap, uint32_t pointer){
	Object * o=heap->heap+pointer;
	uint32_t payloadoffset=pointer + sizeof(Object) + o->refsCount * 4;
	return heap->heap + payloadoffset;
}
