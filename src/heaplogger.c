#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <string.h>
#include <math.h>

#include <sys/ioctl.h>
#include <unistd.h>
#include<time.h>

#include "util/iarraylist.h"
#include "heap.h"
#include "heaplogger.h"

void dumpObj(Heap * h, uint32_t ref){
	Object * o = h->heap+ref;
	printf("obj %d %d ", ref, o->size);
	printf("refs(");
	for(uint16_t i=0;i<o->refsCount;++i)
		printf("%u, ", o->refs[i]);
	printf(")\n");
}

void assertHeap(Heap * heap){
	uint32_t edenSize = heap->edenCapacity - (heap->capacity - heap->pointer);
	if(edenSize > heap->edenCapacity){
		fprintf(stdout, "assertHeap fail: edenSize > edenCapacity\n");
		exit(-1);
	}
}

void printProgress(uint16_t width, uint32_t progress, uint32_t max){
	uint32_t p = (uint32_t)(((double)progress * (double)width) / (double)max);
	for(uint32_t i=0;i<p;++i)
		fprintf(stdout, "|");
	for(uint32_t i=0;i<width-p;++i)
		fprintf(stdout, " ");
}

void printProgressBar(uint16_t width, uint32_t progress, uint32_t max, char * prefix, char * postfix){
	if(progress > max){
		fprintf(stdout, "progress > max\n");
		exit(-1);
		return;
	}
	fprintf(stdout, " [%s", prefix);
	printProgress(width - 4, progress, max);
	fprintf(stdout, "%s]\n\n", postfix);
}

void printHeap(uint16_t width, Heap * heap){
	width-=4;
	uint32_t po = (uint32_t)(((double)heap->oldGenSize * (double)width) / (double)heap->capacity);
	uint32_t poc;
	if(heap->s1==0)
		poc = (uint32_t)(((double)heap->oldGenCapacity * (double)width) / (double)heap->capacity);
	else
		poc = (uint32_t)(((double)(heap->oldGenCapacity + heap->survivorCapacity) * (double)width) / (double)heap->capacity);

	uint32_t ps = poc + (uint32_t)(((double)heap->survivorSize * (double)width) / (double)heap->capacity);
	uint32_t psc;
	if(heap->s1==0)
		psc=poc + (uint32_t)(((double)heap->survivorCapacity * 2 * (double)width) / (double)heap->capacity);
	else
		psc=poc + (uint32_t)(((double)heap->survivorCapacity * 1 * (double)width) / (double)heap->capacity);
	uint32_t edenSize = heap->edenCapacity - (heap->capacity - heap->pointer);
	uint32_t pe = psc+(uint32_t)(((double)edenSize * (double)width) / (double)heap->capacity);
	uint32_t i=0;
	//fprintf(stdout, "%d %d %d %d %d\n", po, poc, ps, psc, pe);
	fprintf(stdout, " [");
	fprintf(stdout, "\033[0;33m");
	for(;i<width&&i<po;++i)
		fprintf(stdout, "|");
	for(;i<width&&i<poc;++i)
		fprintf(stdout, " ");
	fprintf(stdout, "\033[0;34m");
	for(;i<width&&i<ps;++i)
		fprintf(stdout, "|");
	for(;i<width&&i<psc;++i)
		fprintf(stdout, " ");
	fprintf(stdout, "\033[0;32m");
	for(;i<width&&i<pe;++i)
		fprintf(stdout, "|");
	for(;i<width;++i)
		fprintf(stdout, " ");
	fprintf(stdout, "\033[0m");
	fprintf(stdout, "]\n\n");
	
}

static uint64_t currentTimeMillis(){
	struct timespec spec;
	clock_gettime(CLOCK_MONOTONIC, &spec);
	time_t s=spec.tv_sec;
	uint64_t ms = round(spec.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
	if (ms > 999) {
		s++;
		ms = 0;
	}
	return s*1000+ms;
}

uint64_t lastRefresh=0;
void logHeap(Heap * heap){
	//usleep(1000);
	uint64_t now=currentTimeMillis();

	if(now-lastRefresh<30)
		return;
	lastRefresh=now;

	assertHeap(heap);
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	const uint32_t oldWidth =   w.ws_col-4;
	const uint32_t youngWidth = w.ws_col-4;
	fprintf(stdout, "\e[1;1H\e[2J");

	const uint32_t h = w.ws_row;
	const uint32_t pt=h/2-5;

	///*
	//for(uint32_t i=0;i<pt;++i)
	fprintf(stdout, "\n\n");
	//*/

	printHeap(w.ws_col, heap);
	fprintf(stdout, "old generation:\n%s", humanSize(heap->oldGenSize));
	fprintf(stdout, "/%s\n", humanSize(heap->oldGenCapacity));

	printProgressBar(w.ws_col, heap->oldGenSize, heap->oldGenCapacity, "\033[0;33m", "\033[0m");

	fprintf(stdout,"survivors (s%d):\n%s/", heap->s1, humanSize(heap->survivorSize));
	fprintf(stdout,"%s (threshold: %d)\n", humanSize(heap->survivorCapacity), heap->survivorThreshold);
	printProgressBar(w.ws_col, heap->survivorSize, heap->survivorCapacity, "\033[0;34m", "\033[0m");

	uint32_t edenSize = heap->edenCapacity - (heap->capacity - heap->pointer);

	fprintf(stdout,"eden:\n%s/", humanSize(edenSize));
	fprintf(stdout,"%s\n", humanSize(heap->edenCapacity));
	printProgressBar(w.ws_col, edenSize, heap->edenCapacity, "\033[0;32m", "\033[0m");
	fflush(stdout);
}
