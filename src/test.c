#include<stdio.h>
#include<unistd.h>
#include <time.h>
#include<stdbool.h>
#include<stdlib.h>
#include <stdint.h>
#include "util/iarraylist.h"
#include"heap.h"

void assert(uint32_t expected, uint32_t got){
	if(expected==got)return;
	fprintf(stdout, "assert fail: %u!=%u", expected, got);
	exit(-1);
}

uint32_t local1=0;

//callback function to gather gc roots
void beforeGC(Heap * heap, MarkALive callback){
	if(local1!=0)
		callback(heap, &local1);
}

int main(int argc, char *argv[]) {
	int opt;
	uint64_t seed=time(NULL);
	uint32_t  roots=110;
	uint32_t ll_maxlength=128*4;
	uint32_t maxpayload=1024;
	uint32_t edenSize = 1024*1024;
	uint32_t iterations=1024*128;

	while((opt = getopt(argc, argv, "s:l:r:e:i:")) != -1){
		switch(opt){
			case 's':
				seed = atoi(optarg);
				break;
			case 'l':
				ll_maxlength = atoi(optarg);
				break;
			case 'r':
				roots = atoi(optarg);
				if(roots<2)
					roots=2;
				
				break;
			case 'e':
				edenSize = atoi(optarg);
				break;
			case 'i':
				iterations = atoi(optarg);
				break;

		}
	}

  printf("seed: %zu\n", seed);
	srand(seed);
	static char buf[50000]; /* buf must survive until stdout is closed */
	setvbuf ( stdout , buf , _IOFBF , sizeof(buf) );
	Heap * heap=newHeap(edenSize, NULL, &beforeGC);
	for(uint32_t i=0;i<roots;++i){
		addRoot(heap);
	}

	//creates a linkedlkst per iteration
	for(uint32_t j=0;j<iterations;++j){
		uint32_t first=_new(heap, 1, 4);
		set32(heap, first, 0, 0);
		local1=first;
		uint32_t rootIndex = rand()%roots;
		setRoot(heap, rootIndex, first);
		
		uint32_t ll_length=rand()%ll_maxlength+1;
		uint32_t ll_maxpayload=rand() % maxpayload+1;
		uint32_t pos=0;
		for(uint32_t i=0;i<ll_length;++i){
			int refs = rand()%16+1;
			uint32_t payload = rand()%ll_maxpayload;
			//fprintf(stdout, "payload: %u\n", payload);
			if(rand()%102400==42){
				payload=edenSize/2;
				_new(heap, 0, payload);
				_new(heap, 0, payload);
				_new(heap, 0, payload);
				_new(heap, 0, payload);
				_new(heap, 0, payload);
				_new(heap, 0, payload);
				_new(heap, 0, payload);
				_new(heap, 0, payload);
				_new(heap, 0, payload);
				_new(heap, 0, payload);
			}
			if(rand()%10240==0){
				payload = edenSize+10;
			}
			//if(payload==101){
			//	_new(heap, 16, 1024*1024*16);
			//}
			//printf("refs: %d, payload: %d\n", refs, payload);
			if(payload<4)
				payload=4;

			/*
				if(payload>1024*1024)
					fprintf(stdout, "bigpayload");
					*/
			uint32_t next=_new(heap, refs, payload);
			set32(heap, next, 0, ++pos);
			for(uint16_t k=0;k<refs;++k)
				if(getRef(heap, next, k)!=0){

					printf("test failed: getRef() returned not 0 index: %d\n", k);
				
					exit(-1);
				}

			setRef(heap, local1, 0, next);
			local1=next;
		}

		uint32_t current=getRoot(heap, rootIndex);
		assert(get32(heap, current, 0), 0);
		pos=0;
		uint32_t count=0;
		while(true){
			current=getRef(heap, current, 0);
			if(current==0)break;
			assert(get32(heap, current, 0), ++pos);
			++count;
		}

		if(count != ll_length){

			printf("test failed: length of linked list %d != %d \n", count, ll_length);

			exit(-1);
		}

	}
	deleteHeap(heap);
	return 0;
}
