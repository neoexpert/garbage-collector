#ifndef IARRAYLIST_H
#define IARRAYLIST_H

typedef struct IArrayList {
	uint32_t * values;
	uint32_t size;
	uint32_t capacity;
}IArrayList;
IArrayList* newIArrayList(uint32_t initialCapacity);
void deleteIArrayList(IArrayList * iai);
void* IArrayList_remove(IArrayList * iai, uint32_t index);
void IArrayList_add(IArrayList * iai, uint32_t value);
void IArrayList_add64(IArrayList * iai, int64_t value);
void IArrayList_fixSize(IArrayList * ai);

#endif
