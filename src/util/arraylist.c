#include <stdio.h>
#include <stdlib.h>
#include "mymalloc.h"
#include <inttypes.h>
#include <string.h>
#include "arraylist.h"

ArrayList* newArrayList(int initialCapacity){
	ArrayList * al = malloc(sizeof(ArrayList));
	al->size=0;
	al->capacity=initialCapacity;
	al->values = calloc(initialCapacity,sizeof(void *));
	return al;
}

void* ArrayList_remove(ArrayList * al, int index){
	return NULL;
}

void ArrayList_add(ArrayList * al, void * value){
	if(al->size>=al->capacity){
		int newCapacity=(al->capacity*3)/2+1;
		al->values=realloc(al->values,newCapacity*sizeof(void *));
		al->capacity=newCapacity;
	}
	al->values[al->size++]=value;
}
void ArrayList_addAt(ArrayList * al, int index, void * value){
	if(al->size>=al->capacity){
		int newCapacity=(al->capacity*3)/2+1;
		al->values=realloc(al->values,newCapacity*sizeof(void *));
		al->capacity=newCapacity;
	}
	memcpy(al->values+(index+1), al->values+index, (al->size-index)*sizeof(void*));
	al->values[index]=value;
	al->size++;
}

void ArrayList_fixSize(ArrayList * al){
	al->values=realloc(al->values,al->size*sizeof(void *));
	al->capacity=al->size;
}
