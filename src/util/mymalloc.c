#include <stdio.h>
#include <stdlib.h>
#include "mymalloc.h"

void* my_malloc(size_t size, const char *file, int line, const char *func){

    void *p = malloc(size);
    printf ("Allocated = %s, %i, %s, %p[%li]\n", file, line, func, p, size);

    /*Link List functionality goes in here*/

    return p;
}
void my_free(void * p, const char *file, int line, const char *func){
	free(p);
	printf ("freed = %s, %i, %s, %p\n", file, line, func, p);
}
