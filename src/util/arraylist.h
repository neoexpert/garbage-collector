#ifndef ARRAYLIST_H
#define ARRAYLIST_H

typedef struct ArrayList {
	void ** values;
	int size;
	int capacity;
}ArrayList;
ArrayList* newArrayList(int initialCapacity);
void * ArrayList_remove(ArrayList * al, int index);
void ArrayList_add(ArrayList * al, void * value);
void ArrayList_addAt(ArrayList * al, int pos, void * value);
void ArrayList_fixSize(ArrayList * ai);

#endif
