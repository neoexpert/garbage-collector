#ifdef MEMCHECK
#ifndef MYMALLOC_H
#define MYMALLOC_H
typedef struct Alloc{
	size_t size;
	char * file;
	int line;
	char * finc;
} Alloc;

void* my_malloc(size_t size, const char *file, int line, const char *func);
void my_free(void * pointer, const char *file, int line, const char *func);
#define malloc(X) my_malloc( X, __FILE__, __LINE__, __FUNCTION__)
#define free(X) my_free( X, __FILE__, __LINE__, __FUNCTION__)
#endif
#endif
