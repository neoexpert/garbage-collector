#ifndef HEAP_H
#define HEAP_H
#endif
typedef struct {
	uint32_t size;
	uint32_t newAddr;
	uint8_t gc_cycle;
	uint8_t survived;
	uint16_t refsCount;
	uint32_t refs[];
}Object;

typedef struct Heap Heap;
typedef void (*MarkALive)(Heap * heap, uint32_t *);
typedef void (*BeforeGC)(Heap * heap, MarkALive callback);

struct Heap{
	uint32_t capacity;
	uint32_t pointer;
	uint32_t gcTarget;
	uint32_t oldGenSize;
	uint32_t oldGenCapacity;
	uint32_t genThreshold;
	uint32_t edenCapacity;
	uint32_t survivorCapacity;
	uint32_t survivorOffset;
	uint32_t survivorSize;
	uint8_t survivorThreshold;
	bool survivorFull;
	uint32_t youngCapacity;
	uint32_t minReserve;
	uint8_t s1;
	uint8_t gc_cycle;
	void ** ptr;
	BeforeGC beforeGC;
	IArrayList * roots;
	void * heap;
	uint32_t copySize;
	void * copy;
	IArrayList * refsList;
	IArrayList * refsList0;
	IArrayList * refsList1;
};

Heap * newHeap(uint32_t edenCapacity, void ** heapPtr, BeforeGC gcCallback);
void deleteHeap(Heap * heap);

uint32_t addRoot(Heap * heap);
void setRoot(Heap * heap, uint32_t index, uint32_t ref);
uint32_t getRoot(Heap * heap, uint32_t index);
uint32_t _new(Heap * heap, uint16_t refsCount, uint32_t payload);
void setRef(Heap * heap, uint32_t pointer, uint16_t index, uint32_t ref);
uint32_t getRef(Heap * heap, uint32_t pointer, uint16_t index);

void set32(Heap * heap, uint32_t pointer, uint32_t off, uint32_t value);
uint32_t get32(Heap * heap, uint32_t pointer, uint32_t off);

void dumpObj(Heap * h, uint32_t ref);

static const char *humanSize(uint64_t bytes)
{
	char *suffix[] = {"B", "KB", "MB", "GB", "TB"};
	char length = sizeof(suffix) / sizeof(suffix[0]);

	int i = 0;
	double dblBytes = bytes;

	if (bytes > 1024) {
		for (i = 0; (bytes / 1024) > 0 && i<length-1; i++, bytes /= 1024)
			dblBytes = bytes / 1024.0;
	}

	static char output[200];
	sprintf(output, "%.02lf %s", dblBytes, suffix[i]);
	return output;
}
