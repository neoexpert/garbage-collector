#include<stdio.h>
#include<unistd.h>
#include <time.h>
#include<stdbool.h>
#include<stdlib.h>
#include <stdint.h>
#include "util/iarraylist.h"
#include"heap.h"

uint32_t  roots=1024;
uint32_t ll_maxlength=128*4;
uint32_t maxpayload=1024;
uint32_t edenSize = 1024*1024;
uint64_t seed;

char * getLine(void) {
    char * line = malloc(100), * linep = line;
    size_t lenmax = 100, len = lenmax;
    int c;

    if(line == NULL)
        return NULL;

    for(;;) {
        c = fgetc(stdin);
        if(c == EOF)
            break;

        if(--len == 0) {
            len = lenmax;
            char * linen = realloc(linep, lenmax *= 2);

            if(linen == NULL) {
                free(linep);
                return NULL;
            }
            line = linen + (line - linep);
            linep = linen;
        }

        if((*line++ = c) == '\n')
            break;
    }
    *line = '\0';
    return linep;
}

uint32_t local1=0;
void beforeGC(Heap * heap, MarkALive callback){
	if(local1!=0)
		callback(heap, &local1);
}

uint32_t rootIndex = 0;
void processCMD(Heap * heap, char cmd){
	uint32_t payload;
	uint32_t ref;
	switch(cmd){
		case 'd':
			payload=heap->edenCapacity*2;
			rootIndex=(rootIndex+1)%1024;
			ref=_new(heap, 0, payload);
			setRoot(heap, rootIndex, ref);
			fprintf(stdout, "rootIndex: %u\n", rootIndex);
			break;
		case 'c':
			payload=heap->edenCapacity-sizeof(Object);
			rootIndex=(rootIndex+1)%1024;
			ref=_new(heap, 0, payload);
			setRoot(heap, rootIndex, ref);
			fprintf(stdout, "rootIndex: %u\n", rootIndex);
			break;
		case 'h':
			payload=heap->edenCapacity-sizeof(Object);
			payload/=2;
			rootIndex=(rootIndex+1)%1024;
			ref=_new(heap, 0, payload);
			setRoot(heap, rootIndex, ref);
			fprintf(stdout, "rootIndex: %u\n", rootIndex);
			break;
		case 'l':
			for(int j=0;j<128;++j)
			{
							 uint32_t first=_new(heap, 1, 1);
							 local1 = first;
							 rootIndex = rand()%roots;
							 setRoot(heap, rootIndex, first);

							 uint32_t ll_length=rand()%ll_maxlength+1;
							 uint32_t ll_maxpayload=rand() % maxpayload+1;
							 for(uint32_t i=0;i<ll_length;++i){
								 int refs = rand()%16+1;
								 uint32_t payload = rand()%ll_maxpayload;
								 //fprintf(stdout, "payload: %u\n", payload);
								 //if(payload==101){
								 //	_new(heap, 16, 1024*1024*16);
								 //}
								 //printf("refs: %d, payload: %d\n", refs, payload);
								 uint32_t next=_new(heap, refs, payload);
								 for(uint16_t k=0;k<refs;++k)
									 if(getRef(heap, next, k)!=0){

										 printf("test failed: getRef() returned not 0 index: %d\n", k);

										 exit(-1);
									 }

								 setRef(heap, local1, 0, next);
									local1=next;
							 }

							 uint32_t current=getRoot(heap, rootIndex);
							 uint32_t count=0;
							 while(true){
								 current=getRef(heap, current, 0);
								 if(current==0)break;
								 ++count;
							 }

							 if(count != ll_length){

								 printf("test failed: length of linked list %d != %d \n", count, ll_length);

								 exit(-1);
							 }
						 }
	}
}

int main(int argc, char *argv[]) {
	int opt;
	seed=time(NULL);
	edenSize = 1024*1024;

	while((opt = getopt(argc, argv, "s:l:r:e:i:")) != -1){
		switch(opt){
			case 's':
				seed = atoi(optarg);
				break;
			case 'l':
				ll_maxlength = atoi(optarg);
				break;
			case 'r':
				roots = atoi(optarg);
				if(roots<2)
					roots=2;
				
				break;
			case 'e':
				edenSize = atoi(optarg);
				break;

		}
	}

  printf("seed: %zu\n", seed);
	srand(seed);
	Heap * heap=newHeap(edenSize, NULL, &beforeGC);
	for(uint32_t i=0;i<roots;++i){
		addRoot(heap);
	}


	//creates a linkedlkst per iteration
	while(true){
		char *line = NULL;
		fprintf(stdout, "cmd: ");
		line=getLine();
		fprintf(stdout, "line: %s\n", line);
		char lastCMD;
		switch(line[0]){
			case 'e':
				return 0;
			case 'r':
				rootIndex=0;
			case 'd':
			case 'c':
			case 'h':
			case 'l':
				processCMD(heap, line[0]);
				lastCMD=line[0];
				
				continue;
			case '\n':
				processCMD(heap, lastCMD);
				continue;
			default:
				continue;
		}
		free(line);


	}
	deleteHeap(heap);
	return 0;
}
