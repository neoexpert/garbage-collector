#!/bin/bash
set -x
while true; do
	./test -s "$(( ( RANDOM % 1000 )  + 1 ))" -r "$(( ( RANDOM % 100 )  + 200 ))" -l "$(( ( RANDOM % 1000 )  + 100 ))" -e "$(( ( RANDOM % 100 )  + 100 ))" -i "$(( ( RANDOM % 100 )  + 10 ))"
  if [[ "$?" -ne 0 ]]; then
    break
  fi
done
